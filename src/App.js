import './assets/css/style.css';

// assets image
import banner from './assets/img/architect.jpg';
import house2 from './assets/img/house2.jpg';
import house3 from './assets/img/house3.jpg';
import house4 from './assets/img/house4.jpg';
import house5 from './assets/img/house5.jpg';
import map from './assets/img/map.jpg';
import team1 from './assets/img/team1.jpg';
import team2 from './assets/img/team2.jpg';
import team3 from './assets/img/team3.jpg';
import team4 from './assets/img/team4.jpg';

function App() {

  return (
    <div className="App">

      {/* Navbar (sit on top) */}
      <div className='navbar'>
        <div className='navbar-inner'>
          <a href="/" className='nav-link'><b>EDTS</b> TDP Batch #2</a>
          {/* Float links to the right. Hide them on small screens */}
          <div className='nav-group'>
            <a href="/" className='nav-link'>Projects</a>
            <a href="/" className='nav-link'>About</a>
            <a href="/" className='nav-link'>Contact</a>
          </div>
        </div>
      </div>

      {/* Header */}
      <header className='header' id="home">
        <img className='header-hero' src={banner} alt="Architecture" width={1500} height={800} />
        <div className='header-title'>
          <h1>
            <span>
              <b>EDTS</b>
            </span>
            <span>Architects</span>
          </h1>
        </div>
      </header>

      {/* Page content */}
      <div className='article'>
        {/* Project Section */}
        <div className='section' id="projects">
          <h3>Projects</h3>
        </div>
        <div className='section grid-layout project-list'>
          <div className='grid-item'>
            <div className='project-card'>
              <div className='project-title'>Summer House</div>
              <img src={house2} alt="House" />
            </div>
          </div>
          <div className='grid-item'>
            <div className='project-card'>
              <div className='project-title'>Brick House</div>
              <img src={house3} alt="House" />
            </div>
          </div>
          <div className='grid-item'>
            <div className='project-card'>
              <div className='project-title'>Renovated</div>
              <img src={house4} alt="House" />
            </div>
          </div>
          <div className='grid-item'>
            <div className='project-card'>
              <div className='project-title'>Barn House</div>
              <img src={house5} alt="House" />
            </div>
          </div>
          <div className='grid-item'>
            <div className='project-card'>
              <div className='project-title'>Summer House</div>
              <img src={house3} alt="House" />
            </div>
          </div>
          <div className='grid-item'>
            <div className='project-card'>
              <div className='project-title'>Brick House</div>
              <img src={house2} alt="House" />
            </div>
          </div>
          <div className='grid-item'>
            <div className='project-card'>
              <div className='project-title'>Renovated</div>
              <img src={house5} alt="House" />
            </div>
          </div>
          <div className='grid-item'>
            <div className='project-card'>
              <div className='project-title'>Barn House</div>
              <img src={house4} alt="House" />
            </div>
          </div>
        </div>

        {/* About Section */}
        <div className='section'>
          <h3>About</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Excepteur sint
            occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur
            adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat.
          </p>
        </div>
        <div className='section grid-layout team-list'>
          <div className='grid-item team-card'>
            <img src={team1} alt="Jane" />
            <h3 className='title'>Jane Doe</h3>
            <p className='subtitle'>CEO &amp; Founder</p>
            <p className='summary'>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <button className='btn-contact'>Contact</button>
          </div>
          <div className='grid-item team-card'>
            <img src={team2} alt="John" />
            <h3 className='title'>John Doe</h3>
            <p className='subtitle'>Architect</p>
            <p className='summary'>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <button className='btn-contact'>Contact</button>
          </div>
          <div className='grid-item team-card'>
            <img src={team3} alt="Mike" />
            <h3 className='title'>Mike Ross</h3>
            <p className='subtitle'>Architect</p>
            <p className='summary'>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <button className='btn-contact'>Contact</button>
          </div>
          <div className='grid-item team-card'>
            <img src={team4} alt="Dan" />
            <h3 className='title'>Dan Star</h3>
            <p className='subtitle'>Architect</p>
            <p className='summary'>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <button className='btn-contact'>Contact</button>
          </div>
        </div>

        {/* Contact Section */}
        <div className='section'>
          <h3>Contact</h3>
          <p>Lets get in touch and talk about your next project.</p>
          <form>
            <input className="form-control" type="text" placeholder="Name" name="name" />
            <input className="form-control" type="email" placeholder="Email" name="email" />
            <input className="form-control" type="text" placeholder="Subject" name="subject" maxLength={12} />
            <input className="form-control" type="text" placeholder="Comment" name="comment" />
            <button type="submit" className='form-submit'>SEND MESSAGE</button>
          </form>
        </div>

        {/* Image of location/map */}
        <div className='section maps'>
          <img src={map} alt="maps" />
        </div>

        {/* Contact List Section */}
        <div className='section'>
          <h3>Contact List</h3>
          <div className='table-wrapper'>
            <table className='table'>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Country</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Alfred</td>
                  <td>alfreds@gmail.com</td>
                  <td>Germany</td>
                </tr>
                <tr>
                  <td>Reza</td>
                  <td>reza@gmail.com</td>
                  <td>Indonesia</td>
                </tr>
                <tr>
                  <td>Ismail</td>
                  <td>alfreds@gmail.com</td>
                  <td>Turkey</td>
                </tr>
                <tr>
                  <td>Holand</td>
                  <td>holand@gmail.com</td>
                  <td>Germany</td>
                </tr>
              </tbody>
            </table>
          </div>
          
        </div>
        {/* End page content */}
      </div>

      {/* Footer */}
      <footer className='footer'>
        <p>Copyright 2022</p>
      </footer>

    </div >
  );
}

export default App;
